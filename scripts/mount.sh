#!/usr/bin/env bash

# exit if anything fails.
#set -e

if [ "x" == "x$1" ]; then
    echo "usage $0 <full image>"
    exit 1
fi

MNTDIR="mnt"
FULL_IMG=$1

echo "mounting $PART_IMG to $MNTDIR"
mkdir -p $MNTDIR

echo "- mounting"
#fuse-ext2 $PART_IMG $MNTDIR -o uid=0 -o rw+
if command -v guestmount; then
  guestmount -a $FULL_IMG -m /dev/sda2 -m /dev/sda1:/boot $MNTDIR
else
  mount -o loop $FULL_IMG  $MNTDIR
fi
