#!/usr/bin/env bash

echo "copying extra files"

MNTDIR="mnt"

cp templates/ifcfg-interface-eth0 $MNTDIR/etc/network/interfaces.d/eth0
mkdir $MNTDIR/home/pi/Desktop
cp -v mqttstuff/RPI_temp_sensor_test.py $MNTDIR/home/pi/Desktop
cp -v mqttstuff/Freenove_DHT.py $MNTDIR/home/pi/Desktop
cp -v mqttstuff/setup.py $MNTDIR/home/pi/Desktop
cp -v mqttstuff/i2c_lib.py $MNTDIR/home/pi/Desktop
cp -v mqttstuff/lcddriver.py $MNTDIR/home/pi/Desktop

echo "Done copying files."
