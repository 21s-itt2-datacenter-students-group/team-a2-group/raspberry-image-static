import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO
import time
import Freenove_DHT as DHT
import lcddriver
import json

DHTPin = 11
buzzer = 13
grnPin = 16
redPin = 18

GPIO.setmode(GPIO.BOARD)       # use PHYSICAL GPIO Numbering
GPIO.setup(redPin, GPIO.OUT)   # set the Pins to OUTPUT mode
GPIO.setup(grnPin, GPIO.OUT)
GPIO.setup(buzzer,GPIO.OUT)
GPIO.output(redPin, GPIO.LOW)  # make ledPin output LOW level
GPIO.output(grnPin, GPIO.LOW)
GPIO.output(buzzer, GPIO.LOW)

display = lcddriver.lcd()

dht = DHT.DHT(DHTPin)   #create a DHT class object
sumCnt = 0

mqttBroker = "20.185.62.8"
client = mqtt.Client("Client 1")
client.connect(mqttBroker)
print("Connected to the MQTT broker.")
  
while True:
    try:
        sumCnt += 1         #counting number of reading times
        chk = dht.readDHT11()     #read DHT11 and get a return value. Then determine whether data read is normal according to the return value.
        print ("The sumCnt is : %d, \t chk    : %d"%(sumCnt,chk))
        if (chk is dht.DHTLIB_OK):      #read DHT11 and get a return value. Then determine whether data read is normal according to the return value.
            print("Temperature of", str(dht.temperature), "recorded.")
            GPIO.output(grnPin, GPIO.HIGH)
            time.sleep(2)
            GPIO.output(grnPin, GPIO.LOW)
        elif(chk is dht.DHTLIB_ERROR_CHECKSUM): #data check has errors
            print("Error with data check! Data not sent.")
            GPIO.output(redPin, GPIO.HIGH)
            GPIO.output(buzzer, GPIO.HIGH)
            time.sleep(0.1)
            GPIO.output(buzzer, GPIO.LOW)
            time.sleep(0.2)
            GPIO.output(buzzer, GPIO.HIGH)
            time.sleep(0.1)
            GPIO.output(buzzer, GPIO.LOW)
            time.sleep(2)
            GPIO.output(redPin, GPIO.LOW)
            sumCnt -= 1
            continue
        elif(chk is dht.DHTLIB_ERROR_TIMEOUT):  #reading DHT times out
            print("Reading from sensor timed out.")
            GPIO.output(redPin, GPIO.HIGH)
            GPIO.output(buzzer, GPIO.HIGH)
            time.sleep(0.1)
            GPIO.output(buzzer, GPIO.LOW)
            time.sleep(0.2)
            GPIO.output(buzzer, GPIO.HIGH)
            time.sleep(0.1)
            GPIO.output(buzzer, GPIO.LOW)
            time.sleep(2)
            GPIO.output(redPin, GPIO.LOW)
            time.sleep(2)
            GPIO.output(redPin, GPIO.LOW)
            sumCnt -= 1
            continue
        else:               #other errors
            print("Other error!")
            GPIO.output(redPin, GPIO.HIGH)
            time.sleep(2)
            GPIO.output(redPin, GPIO.LOW)
            sumCnt -= 1
            continue

        if dht.temperature > 70:
            GPIO.output(buzzer, GPIO.HIGH)
        else:
            GPIO.output(buzzer, GPIO.LOW)

        display.lcd_display_string("Powerbox 1", 1)
        display.lcd_display_string("Temp: " + str(dht.temperature) + "C",2)
            
        data = {
            "Name" : "Temperature reading",
            "ID" : sumCnt,
            "Temperature" : dht.temperature

        }
        client.publish("Temperature", json.dumps(data))
        print("Temperature of", str(dht.temperature), "published.")
        time.sleep(2)
    except(KeyboardInterrupt):
        client.disconnect(mqttBroker)
        print("Connection disconnected. ")
        GPIO.cleanup()
        break
    
